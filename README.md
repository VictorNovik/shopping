# The project's README


 - How to run application?
 
 Before running application you have to start Elasticsearch and PostgreSQL before. Check src/main/resouces/application.yml for connection details
 
 ./gradlew bootRun -info
 
 - How to run tests?

 You have to have docker service in your environment to run tests!
 ./gradlew test