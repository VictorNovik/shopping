package com.bymdev.vnovik.shoppingmgm.service.impl;

import com.bymdev.vnovik.shoppingmgm.domain.Order;
import com.bymdev.vnovik.shoppingmgm.domain.OrderItem;
import com.bymdev.vnovik.shoppingmgm.domain.Product;
import com.bymdev.vnovik.shoppingmgm.dto.DailyOrderReportDto;
import com.bymdev.vnovik.shoppingmgm.dto.OrderDto;
import com.bymdev.vnovik.shoppingmgm.dto.PageResult;
import com.bymdev.vnovik.shoppingmgm.exception.InconsistentElementSize;
import com.bymdev.vnovik.shoppingmgm.exception.NotFoundException;
import com.bymdev.vnovik.shoppingmgm.mapper.EntityMapper;
import com.bymdev.vnovik.shoppingmgm.repository.jpa.OrderItemJpaRepository;
import com.bymdev.vnovik.shoppingmgm.repository.jpa.OrderJpaRepository;
import com.bymdev.vnovik.shoppingmgm.repository.search.OrderSearchRepository;
import com.bymdev.vnovik.shoppingmgm.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DefaultOrderService implements OrderService {
    private OrderJpaRepository jpaRepository;
    private OrderItemJpaRepository orderItemRepository;
    private OrderSearchRepository searchRepository;
    private EntityMapper mapper;

    public DefaultOrderService(
            OrderJpaRepository orderRepository,
            OrderItemJpaRepository orderItemRepository,
            OrderSearchRepository searchRepository, EntityMapper mapper) {
        this.jpaRepository = orderRepository;
        this.orderItemRepository = orderItemRepository;
        this.searchRepository = searchRepository;
        this.mapper = mapper;
    }

    @Override
    public OrderDto getById(Long id) {
        log.info("Getting order with id {}", id);
        Order persistedOrder = Optional
                .ofNullable(jpaRepository
                        .findOne(id))
                .orElseThrow(() -> new NotFoundException(Order.class.getSimpleName(), id));
        return mapper.orderToDto(persistedOrder);
    }

    @Override
    public OrderDto save(OrderDto dto) {
        Order order = mapper.orderDtoToOrder(dto);

        List<Long> orderItemIds = order.getItems()
                .stream()
                .filter(oi -> oi.getId() != null)
                .map(OrderItem::getId)
                .distinct()
                .collect(Collectors.toList());

        validatePassedOrderItems(order.getItems().size(), orderItemIds.size());

        List<OrderItem> orderItems = orderItemRepository.findAll(orderItemIds);

        validatePassedOrderItems(order.getItems().size(), orderItems.size());

        BigDecimal totalCharge = calculateTotalCharge(orderItems);

        order.setItems(orderItems);
        order.setTotalCharge(totalCharge);
        order.setDate(LocalDate.now());

        OrderDto result = mapper.orderToDto(jpaRepository.save(order));
        order.getItems()
                .stream()
                .map(OrderItem::getProduct)
                .map(Product::getCategory)
                .filter(Objects::nonNull)
                .forEach(category -> category.setProducts(Collections.emptyList()));
        searchRepository.save(order);
        return result;
    }

    private BigDecimal calculateTotalCharge(List<OrderItem> orderItems) {
        Function<OrderItem, BigDecimal> orderItemCharge =
                item -> item
                        .getProduct()
                        .getPrice()
                        .multiply(new BigDecimal(item.getQuantity()));
        return orderItems.stream()
                .map(orderItemCharge)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private void validatePassedOrderItems(int expectedOrderItemsSize, int realOrderItemsSize) {
        if (realOrderItemsSize != expectedOrderItemsSize) {
            throw new InconsistentElementSize(
                    OrderItem.class.getSimpleName(),
                    expectedOrderItemsSize,
                    realOrderItemsSize);
        }
    }

    @Override
    public void deleteById(Long id) {
        jpaRepository.delete(id);
        searchRepository.delete(id);
    }

    @Override
    public PageResult<OrderDto> search(String s, String fragment, Pageable pageable) {
        String queryStringFragment = String.format("%s:%s", s, fragment);
        QueryStringQueryBuilder query = QueryBuilders.queryStringQuery(queryStringFragment);

        log.info("Execute search request with query {}", query);
        Page<Order> searchResult = searchRepository.search(query, pageable);
        log.info("Search request result: page {}, numberOfElements {}", pageable.getPageNumber(), searchResult.getNumberOfElements());

        List<OrderDto> dtos = searchResult.getContent().stream().map(order -> mapper.orderToDto(order)).collect(Collectors.toList());
        return new PageResult<>(
                searchResult.getTotalPages(),
                searchResult.getTotalElements(),
                searchResult.getNumberOfElements(),
                pageable.getPageNumber(),
                dtos);
    }

    @Override
    public List<DailyOrderReportDto> dailyReport() {
        return mapper.dailyReportsToDto(jpaRepository.doDailyReport());
    }
}
