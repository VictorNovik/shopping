package com.bymdev.vnovik.shoppingmgm.service;

public interface BaseEntityService<T, K> {
    T getById(K id);

    T save(T dto);

    void deleteById(K id);
}
