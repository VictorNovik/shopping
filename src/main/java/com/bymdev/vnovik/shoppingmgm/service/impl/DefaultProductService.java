package com.bymdev.vnovik.shoppingmgm.service.impl;

import com.bymdev.vnovik.shoppingmgm.domain.Category;
import com.bymdev.vnovik.shoppingmgm.domain.Product;
import com.bymdev.vnovik.shoppingmgm.dto.ProductDto;
import com.bymdev.vnovik.shoppingmgm.exception.NotFoundException;
import com.bymdev.vnovik.shoppingmgm.mapper.EntityMapper;
import com.bymdev.vnovik.shoppingmgm.repository.jpa.CategoryJpaRepository;
import com.bymdev.vnovik.shoppingmgm.repository.jpa.ProductJpaRepository;
import com.bymdev.vnovik.shoppingmgm.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class DefaultProductService implements ProductService {

    private EntityMapper mapper;
    private ProductJpaRepository jpaRepository;
    private CategoryJpaRepository categoryRepository;

    public DefaultProductService(
            EntityMapper mapper,
            ProductJpaRepository repository, CategoryJpaRepository categoryRepository) {
        this.mapper = mapper;
        this.jpaRepository = repository;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public ProductDto getById(Long id) {
        log.info("Getting product with id {}", id);
        Product persistedProduct = Optional
                .ofNullable(jpaRepository
                        .findOne(id))
                .orElseThrow(() -> new NotFoundException(Product.class.getSimpleName(), id));
        return mapper.productToDto(persistedProduct);
    }

    @Override
    public ProductDto save(ProductDto dto) {
        Product product = mapper.dtoToProduct(dto);

        if (product.getCategory() != null) {
            setCategoryOrFail(product);
        }

        product = jpaRepository.save(product);
        return mapper.productToDto(product);
    }

    private void setCategoryOrFail(Product product) {
        Long categoryId = product.getCategory().getId();
        Category persistedCategory =
                Optional.ofNullable(categoryRepository
                        .findOne(categoryId))
                .orElseThrow(() -> new NotFoundException(Category.class.getName(), categoryId));
        product.setCategory(persistedCategory);
    }

    @Override
    public void deleteById(Long id) {
        log.debug("Delete product with id {}", id);
        jpaRepository.delete(id);
    }

    @Override
    public List<ProductDto> saveAll(List<ProductDto> dtos) {
        List<Product> products = mapper.productDtosToProducts(dtos);
        products = jpaRepository.save(products);
        return mapper.productsToListDto(products);
    }
}
