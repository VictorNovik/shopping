package com.bymdev.vnovik.shoppingmgm.service;

import com.bymdev.vnovik.shoppingmgm.dto.DailyOrderReportDto;
import com.bymdev.vnovik.shoppingmgm.dto.OrderDto;
import com.bymdev.vnovik.shoppingmgm.dto.PageResult;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface OrderService extends BaseEntityService<OrderDto, Long> {
    PageResult<OrderDto> search(String s, String fragment, Pageable pageable);
    List<DailyOrderReportDto> dailyReport();
}
