package com.bymdev.vnovik.shoppingmgm.service.impl;

import com.bymdev.vnovik.shoppingmgm.domain.OrderItem;
import com.bymdev.vnovik.shoppingmgm.domain.Product;
import com.bymdev.vnovik.shoppingmgm.dto.OrderItemDto;
import com.bymdev.vnovik.shoppingmgm.exception.NotFoundException;
import com.bymdev.vnovik.shoppingmgm.mapper.EntityMapper;
import com.bymdev.vnovik.shoppingmgm.repository.jpa.OrderItemJpaRepository;
import com.bymdev.vnovik.shoppingmgm.repository.jpa.ProductJpaRepository;
import com.bymdev.vnovik.shoppingmgm.service.OrderItemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Objects;
import java.util.Optional;

@Service
@Slf4j
public class DefaultOrderItemService implements OrderItemService {
    private OrderItemJpaRepository jpaRepository;
    private ProductJpaRepository productRepository;
    private EntityMapper mapper;

    public DefaultOrderItemService(
            OrderItemJpaRepository jpaRepository,
            ProductJpaRepository productRepository,
            EntityMapper mapper) {
        this.jpaRepository = jpaRepository;
        this.productRepository = productRepository;
        this.mapper = mapper;
    }

    @Override
    public OrderItemDto getById(Long id) {
        log.info("Getting order item with id {}", id);
        OrderItem persistedOrderItem = Optional
                .ofNullable(jpaRepository
                        .findOne(id))
                .orElseThrow(() -> new NotFoundException(OrderItem.class.getSimpleName(), id));
        return mapper.orderItemToDto(persistedOrderItem);
    }

    @Override
    public OrderItemDto save(OrderItemDto dto) {
        Objects.requireNonNull(dto.getProduct().getId());

        Product product = Optional
                .ofNullable(productRepository.findOne(dto.getProduct().getId()))
                .orElseThrow(() -> new NotFoundException(Product.class.getSimpleName(), dto.getProduct().getId()));

        OrderItem orderItem = mapper.orderItemDtoToOrderItem(dto);
        orderItem.setProduct(product);

        orderItem = jpaRepository.save(orderItem);
        return mapper.orderItemToDto(orderItem);
    }

    @Override
    public void deleteById(Long id) {
        jpaRepository.delete(id);
    }
}
