package com.bymdev.vnovik.shoppingmgm.service;

import com.bymdev.vnovik.shoppingmgm.dto.CategoryDto;

public interface CategoryService extends BaseEntityService<CategoryDto, Long> {}
