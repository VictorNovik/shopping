package com.bymdev.vnovik.shoppingmgm.service;

import com.bymdev.vnovik.shoppingmgm.dto.OrderItemDto;

public interface OrderItemService extends BaseEntityService<OrderItemDto, Long> {
}
