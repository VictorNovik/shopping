package com.bymdev.vnovik.shoppingmgm.service;

import com.bymdev.vnovik.shoppingmgm.dto.ProductDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProductService extends BaseEntityService<ProductDto, Long> {
    List<ProductDto> saveAll(List<ProductDto> products);
}
