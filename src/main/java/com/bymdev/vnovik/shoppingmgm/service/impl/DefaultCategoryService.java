package com.bymdev.vnovik.shoppingmgm.service.impl;

import com.bymdev.vnovik.shoppingmgm.domain.Category;
import com.bymdev.vnovik.shoppingmgm.dto.CategoryDto;
import com.bymdev.vnovik.shoppingmgm.exception.EntityConstraintViolationException;
import com.bymdev.vnovik.shoppingmgm.exception.NotFoundException;
import com.bymdev.vnovik.shoppingmgm.mapper.EntityMapper;
import com.bymdev.vnovik.shoppingmgm.repository.jpa.CategoryJpaRepository;
import com.bymdev.vnovik.shoppingmgm.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

@Service
@Slf4j
public class DefaultCategoryService implements CategoryService {
    private CategoryJpaRepository jpaRepository;
    private EntityMapper mapper;

    public DefaultCategoryService(
            CategoryJpaRepository jpaRepository,
            EntityMapper mapper) {
        this.jpaRepository = jpaRepository;
        this.mapper = mapper;
    }

    @Override
    public CategoryDto getById(Long id) {
        log.info("Getting category with id {}", id);
        Category persistedCategory = Optional
                .ofNullable(jpaRepository
                        .findOne(id))
                .orElseThrow(() -> new NotFoundException(Category.class.getSimpleName(), id));
        return mapper.categoryToDto(persistedCategory);
    }

    @Override
    public CategoryDto save(CategoryDto dto) {
        if (dto.getId() != null) {
            CategoryDto innerDto = getById(dto.getId());
            dto.setProducts(innerDto.getProducts());
        } else {
            dto.setProducts(Collections.emptyList());
        }

        Category category = mapper.categoryDtoToCategory(dto);
        category = jpaRepository.save(category);
        return mapper.categoryToDto(category);
    }

    @Override
    public void deleteById(Long id) {
        Category category = Optional
                .ofNullable(jpaRepository.findOne(id))
                .orElseThrow(() -> new NotFoundException(Category.class.getSimpleName(), id));
        if (category.getProducts().size() > 0) {
            throw new EntityConstraintViolationException(Category.class.getSimpleName(), id);
        }
        jpaRepository.delete(id);
    }
}
