package com.bymdev.vnovik.shoppingmgm.repository.jpa;

import com.bymdev.vnovik.shoppingmgm.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryJpaRepository extends JpaRepository<Category, Long> {
}
