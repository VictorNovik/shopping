package com.bymdev.vnovik.shoppingmgm.repository.jpa;

import com.bymdev.vnovik.shoppingmgm.domain.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderItemJpaRepository extends JpaRepository<OrderItem, Long> {
}
