package com.bymdev.vnovik.shoppingmgm.repository.search;

import com.bymdev.vnovik.shoppingmgm.domain.Order;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface OrderSearchRepository extends ElasticsearchRepository<Order, Long> {
}
