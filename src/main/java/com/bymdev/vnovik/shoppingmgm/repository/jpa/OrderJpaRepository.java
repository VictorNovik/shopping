package com.bymdev.vnovik.shoppingmgm.repository.jpa;

import com.bymdev.vnovik.shoppingmgm.domain.DailyOrderReport;
import com.bymdev.vnovik.shoppingmgm.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderJpaRepository extends JpaRepository<Order, Long> {
    @Query(value = "SELECT new com.bymdev.vnovik.shoppingmgm.domain.DailyOrderReport(o.date, sum(o.totalCharge)) from Order o group by o.date")
    List<DailyOrderReport> doDailyReport();
}
