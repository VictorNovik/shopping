package com.bymdev.vnovik.shoppingmgm.repository.jpa;

import com.bymdev.vnovik.shoppingmgm.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface ProductJpaRepository extends JpaRepository<Product, Long> {
}
