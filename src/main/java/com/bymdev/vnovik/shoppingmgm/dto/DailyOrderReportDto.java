package com.bymdev.vnovik.shoppingmgm.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DailyOrderReportDto {
    private String date;
    private BigDecimal income;
}
