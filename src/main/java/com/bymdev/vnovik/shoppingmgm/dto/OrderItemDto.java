package com.bymdev.vnovik.shoppingmgm.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderItemDto implements Serializable {
    private Long id;
    @NotNull
    @Min(value = 1)
    private Integer quantity;
    @NotNull
    private ProductDto product;
}
