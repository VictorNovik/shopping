package com.bymdev.vnovik.shoppingmgm.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PageResult<T> implements Serializable {
    private int totalPages;
    private long totalElements;
    private int numberOfElements;
    private int pageNumber;

    private List<T> elements;
}
