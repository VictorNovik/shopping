package com.bymdev.vnovik.shoppingmgm.dto;

import com.bymdev.vnovik.shoppingmgm.domain.Product;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {
    private Long id;
    private BigDecimal price;
    @NotNull
    private String sku;
    private String name;
    private CategoryDto category;
}
