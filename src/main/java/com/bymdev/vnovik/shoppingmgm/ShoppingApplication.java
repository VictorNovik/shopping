package com.bymdev.vnovik.shoppingmgm;

import com.bymdev.vnovik.shoppingmgm.domain.Product;
import com.bymdev.vnovik.shoppingmgm.util.MDCUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.bymdev.vnovik.shoppingmgm")
@EntityScan(basePackageClasses = {Product.class, Jsr310JpaConverters.class})
public class ShoppingApplication {
    public static void main(String[] args) {
        MDCUtil.runWithRid(() -> SpringApplication.run(ShoppingApplication.class, args));
    }
}
