package com.bymdev.vnovik.shoppingmgm.rest;

import com.bymdev.vnovik.shoppingmgm.dto.ProductDto;
import com.bymdev.vnovik.shoppingmgm.service.ProductService;
import io.swagger.annotations.*;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController()
@RequestMapping(path = "/api/products")
public class ProductResource {
    private ProductService service;

    public ProductResource(ProductService service) {
        this.service = service;
    }

    @GetMapping(path = "/{id}")
    @ResponseBody
    @ApiOperation(value = "Get product by id", response = ProductDto.class)
    public ResponseEntity<ProductDto> getOne(@PathVariable("id") Long id) {
        return ResponseEntity.ok(service.getById(id));
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @ApiResponse(code = 200, response = ProductDto.class, message = "Updated product")
    @ApiOperation(value = "Update or add product", response = ProductDto.class)
    public ResponseEntity<ProductDto> update(
            @ApiParam(name = "Product object", value = "Product body to update")
            @RequestBody
            @Valid ProductDto dto) {
        return ResponseEntity.ok(service.save(dto));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @ApiOperation(value = "Add product", response = ProductDto.class, notes = "Product id has not to be passed")
    @ApiModelProperty(example = "{\"name\": \"string\",\"price\": 0,\"sku\": \"string\"}")
    @ApiResponse(code = 200, response = ProductDto.class, message = "Added product with id")
    public ResponseEntity<ProductDto> add(
            @ApiParam(name = "Product object", value = "Product body to add")
            @RequestBody
            @Valid ProductDto dto) {
        if (dto.getId() != null) {
            throw new IllegalArgumentException("Product id was passed in the product's object. Please use PUT method to update existed product");
        }

        return ResponseEntity.ok(service.save(dto));
    }

    @DeleteMapping(path = "/{id}")
    @ApiOperation(value = "Delete product by id")
    @ApiResponse(code = 200, message = "Entity was deleted")
    public void delete(@PathVariable Long id) {
        service.deleteById(id);
    }
}
