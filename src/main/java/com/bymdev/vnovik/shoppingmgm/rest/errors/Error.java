package com.bymdev.vnovik.shoppingmgm.rest.errors;

import lombok.Getter;

import java.io.Serializable;

/**
 * View Model for transferring error message with a list of field errors.
 */

@Getter
public class Error implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String message;
    private final String description;

    public Error(String message) {
        this(message, null);
    }

    public Error(String message, String description) {
        this.message = message;
        this.description = description;
    }
}
