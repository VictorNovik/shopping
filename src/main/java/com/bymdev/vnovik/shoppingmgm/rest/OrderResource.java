package com.bymdev.vnovik.shoppingmgm.rest;

import com.bymdev.vnovik.shoppingmgm.dto.OrderDto;
import com.bymdev.vnovik.shoppingmgm.dto.PageResult;
import com.bymdev.vnovik.shoppingmgm.service.OrderService;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController()
@RequestMapping(path = "/api/orders")
public class OrderResource {

    @Value("${search.page.size:100}")
    public int defaultPageSize;
    private OrderService service;

    public OrderResource(OrderService service) {
        this.service = service;
    }

    @GetMapping(path = "/{id}")
    @ResponseBody
    @ApiOperation(value = "Get order by id", response = OrderDto.class)
    public ResponseEntity<OrderDto> getOne(@PathVariable("id") Long id) {
        return ResponseEntity.ok(service.getById(id));
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @ApiResponse(code = 200, response = OrderDto.class, message = "Updated order")
    @ApiOperation(value = "Update or add order", response = OrderDto.class)
    public ResponseEntity<OrderDto> update(
            @ApiParam(name = "Order object", value = "Order body to update")
            @RequestBody
            @Valid OrderDto dto) {
        return ResponseEntity.ok(service.save(dto));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @ApiOperation(value = "Add order", response = OrderDto.class, notes = "Order id has not to be passed")
    @ApiModelProperty(example = "{\"name\": \"string\",\"price\": 0,\"sku\": \"string\"}")
    @ApiResponse(code = 200, response = OrderDto.class, message = "Added order with id")
    public ResponseEntity<OrderDto> add(
            @ApiParam(name = "Order object", value = "Order body to add")
            @RequestBody
            @Valid OrderDto dto) {
        if (dto.getId() != null) {
            throw new IllegalArgumentException("Order id was passed in the order's object. Please use PUT method to update existed order");
        }

        return ResponseEntity.ok(service.save(dto));
    }

    @DeleteMapping(path = "/{id}")
    @ApiOperation(value = "Delete order by id")
    @ApiResponse(code = 200, message = "Entity was deleted")
    public void delete(@PathVariable Long id) {
        service.deleteById(id);
    }

    @PostMapping(path = "_search/by-product-name")
    @ResponseBody
    @ApiOperation(value = "Find orders by product fragment with paging")
    public ResponseEntity<PageResult<OrderDto>> searchByProductFragment(
            @RequestParam @ApiParam String fragment,
            @ApiParam @RequestParam(required = false) Pageable pageable) {

        Pageable innerPage = pageable;
        if (innerPage == null) {
            innerPage = new PageRequest(0, defaultPageSize, Sort.Direction.ASC, "id");
        }
        PageResult<OrderDto> result = service.search("items.product.name", fragment, innerPage);
        return ResponseEntity.ok(result);
    }
}
