package com.bymdev.vnovik.shoppingmgm.rest;

import com.bymdev.vnovik.shoppingmgm.dto.OrderItemDto;
import com.bymdev.vnovik.shoppingmgm.service.OrderItemService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/order-items")
public class OrderItemResource {
    private OrderItemService service;

    public OrderItemResource(OrderItemService service) {
        this.service = service;
    }

    @GetMapping(path = "/{id}")
    @ResponseBody
    @ApiOperation(value = "Get order item by id", response = OrderItemDto.class)
    public ResponseEntity<OrderItemDto> getOne(@PathVariable("id") Long id) {
        return ResponseEntity.ok(service.getById(id));
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @ApiResponse(code = 200, response = OrderItemDto.class, message = "Updated order item")
    @ApiOperation(value = "Update or add order item", response = OrderItemDto.class)
    public ResponseEntity<OrderItemDto> update(
            @ApiParam(name = "Order item object", value = "Order item body to update")
            @RequestBody
            @Valid OrderItemDto dto) {
        return ResponseEntity.ok(service.save(dto));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @ApiOperation(value = "Add order item", response = OrderItemDto.class, notes = "Order item id has not to be passed")
    @ApiResponse(code = 200, response = OrderItemDto.class, message = "Added order item with id")
    public ResponseEntity<OrderItemDto> add(
            @ApiParam(name = "Order item object", value = "Order item body to add")
            @RequestBody
            @Valid OrderItemDto dto) {
        if (dto.getId() != null) {
            throw new IllegalArgumentException("Order item id was passed in the order item's object. Please use PUT method to update existed order item");
        }

        return ResponseEntity.ok(service.save(dto));
    }

    @DeleteMapping(path = "/{id}")
    @ApiOperation(value = "Delete order item by id")
    @ApiResponse(code = 200, message = "Entity was deleted")
    public void delete(@PathVariable Long id) {
        service.deleteById(id);
    }
}
