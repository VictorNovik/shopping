package com.bymdev.vnovik.shoppingmgm.rest.errors;

public interface ErrorConstants {
    String ERR_METHOD_NOT_SUPPORTED = "error.methodNotSupported";
    String ERR_INTERNAL_SERVER_ERROR = "error.internalServerError";
    String ERR_CONFLICT = "error.conflict";
    String ERR_NOT_FOUND = "error.notFound";
    String ERR_INCONSISTENT_SIZE = "error.inconsistentSize";
}
