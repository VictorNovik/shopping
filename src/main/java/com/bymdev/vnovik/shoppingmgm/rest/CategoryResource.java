package com.bymdev.vnovik.shoppingmgm.rest;

import com.bymdev.vnovik.shoppingmgm.dto.CategoryDto;
import com.bymdev.vnovik.shoppingmgm.service.CategoryService;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController()
@RequestMapping(path = "/api/categories")
public class CategoryResource {
    private CategoryService service;

    public CategoryResource(CategoryService service) {
        this.service = service;
    }

    @GetMapping(path = "/{id}")
    @ResponseBody
    @ApiOperation(value = "Get category by id", response = CategoryDto.class)
    public ResponseEntity<CategoryDto> getOne(@PathVariable("id") Long id) {
        return ResponseEntity.ok(service.getById(id));
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @ApiResponse(code = 200, response = CategoryDto.class, message = "Updated category")
    @ApiOperation(value = "Update or add category", response = CategoryDto.class)
    public ResponseEntity<CategoryDto> update(
            @ApiParam(name = "Category object", value = "Category body to update")
            @RequestBody
            @Valid CategoryDto dto) {
        return ResponseEntity.ok(service.save(dto));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @ApiOperation(value = "Add category", response = CategoryDto.class, notes = "Category id has not to be passed")
    @ApiResponse(code = 200, response = CategoryDto.class, message = "Added category with id")
    public ResponseEntity<CategoryDto> add(
            @ApiParam(name = "Category object", value = "Category body to add")
            @RequestBody
            @Valid CategoryDto dto) {
        if (dto.getId() != null) {
            throw new IllegalArgumentException("Category id was passed in the category's object. Please use PUT method to update existed category");
        }

        return ResponseEntity.ok(service.save(dto));
    }

    @DeleteMapping(path = "/{id}")
    @ApiOperation(value = "Delete category by id")
    @ApiResponse(code = 200, message = "Entity was deleted")
    public void delete(@PathVariable Long id) {
        service.deleteById(id);
    }
}
