package com.bymdev.vnovik.shoppingmgm.rest;

import com.bymdev.vnovik.shoppingmgm.dto.DailyOrderReportDto;
import com.bymdev.vnovik.shoppingmgm.service.OrderService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/report")
public class ReportingResource {
    private OrderService orderService;

    public ReportingResource(OrderService orderService) {
        this.orderService = orderService;
    }

    @ApiOperation(value = "Get daily orders report")
    @ResponseBody
    @GetMapping(path = "daily", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<DailyOrderReportDto>> dailyReport() {
        return ResponseEntity.ok(orderService.dailyReport());
    }
}
