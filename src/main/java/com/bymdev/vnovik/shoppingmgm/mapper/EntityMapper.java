package com.bymdev.vnovik.shoppingmgm.mapper;

import com.bymdev.vnovik.shoppingmgm.domain.*;
import com.bymdev.vnovik.shoppingmgm.dto.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EntityMapper {
    @Mapping(target = "category.products", ignore = true)
    ProductDto productToDto(Product product);

    Product dtoToProduct(ProductDto dto);

    List<ProductDto> productsToListDto(List<Product> products);

    CategoryDto categoryToDto(Category category);

    List<Product> productDtosToProducts(List<ProductDto> dtos);

    Category categoryDtoToCategory(CategoryDto dto);

    OrderItemDto orderItemToDto(OrderItem orderItem);

    OrderItem orderItemDtoToOrderItem(OrderItemDto dto);

    OrderDto orderToDto(Order order);

    Order orderDtoToOrder(OrderDto dto);

    List<DailyOrderReportDto> dailyReportsToDto(List<DailyOrderReport> dailyOrderReports);
}
