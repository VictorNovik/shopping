package com.bymdev.vnovik.shoppingmgm.exception;

public class NotFoundException extends RuntimeException {
    public NotFoundException(String objName, Long id) {
        super(String.format("Entity %s with id %d was not found", objName, id));
    }
}
