package com.bymdev.vnovik.shoppingmgm.exception;

public class EntityConstraintViolationException extends RuntimeException {
    public EntityConstraintViolationException(String entityName, Long id) {
        super(String.format("Entity %s with id %d has references to other entities", entityName, id));
    }
}
