package com.bymdev.vnovik.shoppingmgm.exception;

public class InconsistentElementSize extends RuntimeException {
    public InconsistentElementSize(
            String entityName,
            int expectedSize,
            int realSize) {
        super(String.format("Inconsistent element size of %s, expected %d but real %d",
                entityName, expectedSize, realSize));
    }
}
