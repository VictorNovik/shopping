package com.bymdev.vnovik.shoppingmgm.util;

import org.apache.logging.log4j.ThreadContext;

import java.time.ZoneId;
import java.util.concurrent.atomic.AtomicLong;

public final class MDCUtil {


    /** */
    private static final AtomicLong ridSalt = new AtomicLong(0);

    /**
     * Instantiates a new mDC util.
     */
    private MDCUtil() {
        super();
    }

    /**
     * The Enum MDCKEY.
     */
    public enum MDCKEY {

        /** The USER. */
        USER("user"),

        /** The RID. */
        RID("rid"),
        
        /** */
        TIME_ID("timeId");

        /** The key. */
        private String key;

        /**
         * Instantiates a new mDCKEY.
         * @param key the key
         */
        private MDCKEY(final String key) {
            this.key = key;
        }

        /**
         * Gets the key.
         * @return key.
         */
        public String getKey() {
            return key;
        }
    }

    /**
     * Put data.
     * @param key the key
     * @param data the data
     */
    public static void putData(final MDCKEY key, final String data) {
        if (key != null && data != null) {
            ThreadContext.put(key.getKey(), data);
        }
    }

    public static void setRid() {
        putData(MDCKEY.RID, "" + (System.currentTimeMillis() + ridSalt.incrementAndGet()));
    }
    
    public static ZoneId getZoneId() {
        String id = getData(MDCKEY.TIME_ID);
        return id != null ? ZoneId.of(id) : ZoneId.systemDefault();
    }

    /**
     * Removes the data.
     * @param key the key
     */
    public static void removeData(final MDCKEY key) {
        ThreadContext.remove(key.getKey());
    }

    /**
     * Removes the all data.
     */
    public static void removeAllData() {
        ThreadContext.clearMap();
    }

    /**
     * Gets the data.
     * @param key the key
     * @return the data
     */
    public static String getData(final MDCKEY key) {
        return ThreadContext.get(key.getKey());
    }

    public static void runWithRid(Task method) {
        try {
            MDCUtil.setRid();
            method.run();
        } finally {
            MDCUtil.removeAllData();
        }
    }

    public interface Task {
        void run();
    }
    
}
