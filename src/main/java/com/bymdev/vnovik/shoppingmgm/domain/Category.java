package com.bymdev.vnovik.shoppingmgm.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@Entity(name = "categories")
@NoArgsConstructor
@AllArgsConstructor
public class Category {
    @Id
    @GenericGenerator(
            name="category_id_sequence",
            strategy = "sequence",
            parameters = { @Parameter(name="sequence", value="category_pk_seq") } )
    @GeneratedValue(generator = "category_id_sequence")
    private Long id;
    private String name;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "categories_products",
            joinColumns = { @JoinColumn(name = "category_id", referencedColumnName = "id") },
            inverseJoinColumns = { @JoinColumn(name = "product_id", referencedColumnName = "id") })
    private List<Product> products;
}
