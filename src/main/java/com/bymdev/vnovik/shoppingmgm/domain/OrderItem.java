package com.bymdev.vnovik.shoppingmgm.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;

@Data
@Builder
@Entity(name = "order_items")
@NoArgsConstructor
@AllArgsConstructor
public class OrderItem {
    @Id
    @GenericGenerator(
            name="order_item_id_sequence",
            strategy = "sequence",
            parameters = { @Parameter(name="sequence", value="order_item_pk_seq") } )
    @GeneratedValue(generator = "order_item_id_sequence")
    private Long id;
    private Integer quantity;
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;
}
