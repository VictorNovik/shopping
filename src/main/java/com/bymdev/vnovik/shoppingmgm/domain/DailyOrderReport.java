package com.bymdev.vnovik.shoppingmgm.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DailyOrderReport implements Serializable {
    private LocalDate date;
    private BigDecimal income;
}
