package com.bymdev.vnovik.shoppingmgm.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Builder
@Entity(name = "products")
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    @Id
    @GenericGenerator(
            name="product_id_sequence",
            strategy = "sequence",
            parameters = { @org.hibernate.annotations.Parameter(name="sequence", value="product_pk_seq") } )
    @GeneratedValue(generator = "product_id_sequence")
    private Long id;
    @Field(type = FieldType.Double)
    @JsonProperty()
    private BigDecimal price;
    private String sku;
    private String name;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinTable(
            name = "categories_products",
            joinColumns = { @JoinColumn(name = "product_id", referencedColumnName = "id") },
            inverseJoinColumns = { @JoinColumn(name = "category_id", referencedColumnName = "id") })
    private Category category;
}
