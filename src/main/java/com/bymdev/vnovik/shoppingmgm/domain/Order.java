package com.bymdev.vnovik.shoppingmgm.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Document(indexName = "orders", type = "order")
@Table(name = "orders")
public class Order {
    @Id
    @GenericGenerator(
            name = "order_id_sequence",
            strategy = "sequence",
            parameters = { @Parameter(name = "sequence", value = "order_pk_seq")})
    @GeneratedValue(generator = "order_id_sequence")
    private Long id;
    @OneToMany
    @JoinTable(
            name = "order_order_items",
            joinColumns = { @JoinColumn(name = "order_id", referencedColumnName = "id") },
            inverseJoinColumns = { @JoinColumn(name = "order_item_id", referencedColumnName = "id") }
    )
    private List<OrderItem> items;
    private BigDecimal totalCharge;
    @Column(name = "order_date")
    private LocalDate date;
}
