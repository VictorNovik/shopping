package com.bymdev.vnovik.shoppingmgm.rest;

import com.bymdev.vnovik.shoppingmgm.ShoppingApplication;
import com.bymdev.vnovik.shoppingmgm.domain.Category;
import com.bymdev.vnovik.shoppingmgm.domain.OrderItem;
import com.bymdev.vnovik.shoppingmgm.domain.Product;
import com.bymdev.vnovik.shoppingmgm.dto.CategoryDto;
import com.bymdev.vnovik.shoppingmgm.dto.OrderItemDto;
import com.bymdev.vnovik.shoppingmgm.dto.ProductDto;
import com.bymdev.vnovik.shoppingmgm.mapper.EntityMapper;
import com.bymdev.vnovik.shoppingmgm.repository.jpa.CategoryJpaRepository;
import com.bymdev.vnovik.shoppingmgm.repository.jpa.OrderItemJpaRepository;
import com.bymdev.vnovik.shoppingmgm.rest.errors.ExceptionTranslator;
import com.bymdev.vnovik.shoppingmgm.service.CategoryService;
import com.bymdev.vnovik.shoppingmgm.service.OrderItemService;
import com.bymdev.vnovik.shoppingmgm.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Collections;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ShoppingApplication.class})
@Slf4j
public class OrderItemResourceIntTest {
    private MockMvc mockMvc;

    @Autowired
    private OrderItemResource orderItemResource;

    @Autowired
    private OrderItemJpaRepository orderItemRepository;

    @Autowired
    private OrderItemService orderItemService;

    @Autowired
    private ProductService productService;

    @Autowired
    private EntityMapper mapper;

    @Autowired
    private ObjectMapper objectMapper;

    private OrderItem defaultOrderItem;
    private OrderItemDto defaultOrderItemDto;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(orderItemResource)
                .setControllerAdvice(new ExceptionTranslator())
                .build();

        orderItemRepository.deleteAll();
        ProductDto product = ProductDto
                .builder()
                .name("name")
                .price(new BigDecimal("4.4"))
                .sku("sku")
                .build();

        product = productService.save(product);
        defaultOrderItem = OrderItem.builder()
                .quantity(2)
                .product(mapper.dtoToProduct(product))
                .build();
        defaultOrderItemDto = mapper.orderItemToDto(defaultOrderItem);
    }

    @After
    public void afterMethod() {
        defaultOrderItem = null;
    }

    @Test
    @Transactional
    public void shouldGetOrderItemById() throws Exception {
        OrderItemDto storedOrderItem = orderItemService.save(defaultOrderItemDto);
        mockMvc.perform(
                MockMvcRequestBuilders
                        .get("/api/order-items/" + storedOrderItem.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(storedOrderItem.getId()))
                .andExpect(jsonPath("$.quantity").value(storedOrderItem.getQuantity()))
                .andExpect(jsonPath("$.product.id").value(storedOrderItem.getProduct().getId()));
    }

    @Test
    @Transactional
    public void shouldAddNewOrderItem() throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/api/order-items")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(defaultOrderItemDto)))
                .andExpect(status().isOk())
                .andReturn();
        String responseBodyJson = mvcResult.getResponse().getContentAsString();
        Long storedOrderItemId = JsonPath.parse(responseBodyJson).read("$.id", Long.class);
        OrderItem persistedOrderItem = orderItemRepository.findOne(storedOrderItemId);

        Assert.assertNotNull(persistedOrderItem);
        Assert.assertEquals(defaultOrderItemDto.getQuantity(), persistedOrderItem.getQuantity());
        Assert.assertNotNull(persistedOrderItem.getProduct());
        Assert.assertEquals(defaultOrderItemDto.getProduct().getId(), persistedOrderItem.getProduct().getId());
    }

    @Test
    @Transactional
    public void shouldUpdateExistedOrderItem() throws Exception {
        OrderItemDto persistedOrderItem = orderItemService.save(defaultOrderItemDto);
        OrderItemDto newOrderItem = OrderItemDto
                .builder()
                .id(persistedOrderItem.getId())
                .quantity(555)
                .product(persistedOrderItem.getProduct())
                .build();

        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .put("/api/order-items")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(newOrderItem)))
                .andExpect(status().isOk())
                .andReturn();

        OrderItem updatedOrderItem = orderItemRepository.findOne(persistedOrderItem.getId());

        Assert.assertNotNull(updatedOrderItem);
        Assert.assertEquals(newOrderItem.getQuantity(), updatedOrderItem.getQuantity());
        Assert.assertNotNull(updatedOrderItem.getProduct());
    }

    @Test
    @Transactional
    public void shouldDeleteCategoryWithoutProductsById() throws Exception {
        OrderItemDto storedOrderItem = orderItemService.save(defaultOrderItemDto);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .delete("/api/order-items/" + storedOrderItem.getId()))
                .andExpect(status().isOk());

        OrderItem removedProduct = orderItemRepository.findOne(storedOrderItem.getId());
        Assert.assertNull(removedProduct);
    }
}
