package com.bymdev.vnovik.shoppingmgm.rest;

import com.bymdev.vnovik.shoppingmgm.ShoppingApplication;
import com.bymdev.vnovik.shoppingmgm.domain.Category;
import com.bymdev.vnovik.shoppingmgm.domain.Product;
import com.bymdev.vnovik.shoppingmgm.dto.CategoryDto;
import com.bymdev.vnovik.shoppingmgm.dto.ProductDto;
import com.bymdev.vnovik.shoppingmgm.mapper.EntityMapper;
import com.bymdev.vnovik.shoppingmgm.repository.jpa.CategoryJpaRepository;
import com.bymdev.vnovik.shoppingmgm.repository.jpa.ProductJpaRepository;
import com.bymdev.vnovik.shoppingmgm.rest.errors.ExceptionTranslator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.transaction.Transactional;
import java.math.BigDecimal;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ShoppingApplication.class})
@Slf4j
public class ProductResourceIntTest {
    private MockMvc mockMvc;

    @Autowired
    private ProductResource productResource;

    @Autowired
    private ProductJpaRepository productRepository;

    @Autowired
    private CategoryJpaRepository categoryRepository;

    @Autowired
    private EntityMapper mapper;

    @Autowired
    private ObjectMapper objectMapper;

    private Product defaultProduct;
    private ProductDto defaultProductDto;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(productResource)
                .setControllerAdvice(new ExceptionTranslator())
                .build();

        productRepository.deleteAll();
        defaultProduct = Product.builder()
                .name("Default name")
                .sku("Default SKU")
                .price(new BigDecimal("12.49"))
                .build();
        defaultProductDto = mapper.productToDto(defaultProduct);
    }

    @After
    public void afterMethod() {
        defaultProduct = null;
    }

    @Test
    @Transactional
    public void shouldGetProductById() throws Exception {
        Product storedProduct = productRepository.save(defaultProduct);
        ProductDto dto = mapper.productToDto(storedProduct);
        mockMvc.perform(
                MockMvcRequestBuilders
                        .get("/api/products/" + dto.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(dto.getId()))
                .andExpect(jsonPath("$.name").value(dto.getName()))
                .andExpect(jsonPath("$.sku").value(dto.getSku()))
                .andExpect(jsonPath("$.price").value(dto.getPrice()));
    }

    @Test
    @Transactional
    public void shouldAddNewProduct() throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/api/products")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(defaultProductDto)))
                .andExpect(status().isOk())
                .andReturn();
        String responseBodyJson = mvcResult.getResponse().getContentAsString();
        Long storedProductId = JsonPath.parse(responseBodyJson).read("$.id", Long.class);
        Product persistedProduct = productRepository.findOne(storedProductId);

        Assert.assertNotNull(persistedProduct);
        Assert.assertEquals(defaultProductDto.getName(), persistedProduct.getName());
        Assert.assertEquals(defaultProductDto.getPrice(), persistedProduct.getPrice());
        Assert.assertEquals(defaultProductDto.getSku(), persistedProduct.getSku());
    }

    @Test
    @Transactional
    public void shouldAddNewProductWithCategory() throws Exception {
        Category persistedCategory = categoryRepository.save(
                Category
                        .builder()
                        .name("category name")
                        .build()
        );

        defaultProductDto.setCategory(mapper.categoryToDto(persistedCategory));
        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/api/products")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(defaultProductDto)))
                .andExpect(status().isOk())
                .andReturn();
        String responseBodyJson = mvcResult.getResponse().getContentAsString();
        Long storedProductId = JsonPath.parse(responseBodyJson).read("$.id", Long.class);
        Product persistedProduct = productRepository.findOne(storedProductId);

        Assert.assertNotNull(persistedProduct);
        Assert.assertEquals(defaultProductDto.getName(), persistedProduct.getName());
        Assert.assertEquals(defaultProductDto.getPrice(), persistedProduct.getPrice());
        Assert.assertEquals(defaultProductDto.getSku(), persistedProduct.getSku());
        Assert.assertNotNull(persistedProduct.getCategory());
        Assert.assertEquals(persistedCategory.getId(), persistedProduct.getCategory().getId());
    }

    @Test
    @Transactional
    public void shouldUpdateExistedProduct() throws Exception {
        Product persistedProduct = productRepository.save(defaultProduct);
        persistedProduct.setName("Updated name");
        persistedProduct.setPrice(new BigDecimal("44.44"));
        persistedProduct.setSku("Updated sku");

        ProductDto persistedProductDto = mapper.productToDto(persistedProduct);

        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .put("/api/products")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(persistedProductDto)))
                .andExpect(status().isOk())
                .andReturn();

        Product updatedProduct = productRepository.findOne(persistedProduct.getId());

        Assert.assertNotNull(updatedProduct);
        Assert.assertEquals(persistedProduct.getName(), updatedProduct.getName());
        Assert.assertEquals(persistedProduct.getPrice(), updatedProduct.getPrice());
        Assert.assertEquals(persistedProduct.getSku(), updatedProduct.getSku());
    }

    @Test
    @Transactional
    public void shouldDeleteProductById() throws Exception {
        Product storedProduct = productRepository.save(defaultProduct);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .delete("/api/products/" + storedProduct.getId()))
                .andExpect(status().isOk());

        Product removedProduct = productRepository.findOne(storedProduct.getId());
        Assert.assertNull(removedProduct);
    }
}
