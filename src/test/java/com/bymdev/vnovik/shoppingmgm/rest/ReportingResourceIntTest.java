package com.bymdev.vnovik.shoppingmgm.rest;

import com.bymdev.vnovik.shoppingmgm.ShoppingApplication;
import com.bymdev.vnovik.shoppingmgm.domain.Order;
import com.bymdev.vnovik.shoppingmgm.repository.jpa.OrderJpaRepository;
import com.bymdev.vnovik.shoppingmgm.rest.errors.ExceptionTranslator;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;

import static org.hamcrest.Matchers.hasItems;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ShoppingApplication.class})
@Slf4j
public class ReportingResourceIntTest {
    private MockMvc mockMvc;

    @Autowired
    private ReportingResource reportingResource;

    @Autowired
    private OrderJpaRepository orderRepository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(reportingResource)
                .setControllerAdvice(new ExceptionTranslator())
                .build();

        orderRepository.deleteAll();
    }

    @Test
    @Transactional
    public void shouldGetDailyReport() throws Exception {
        LocalDate today = LocalDate.now();
        Order firstTodayOrder = orderRepository.save(
                Order
                        .builder()
                        .totalCharge(new BigDecimal("4.2"))
                        .date(today)
                        .build()
        );

        Order secondTodayOrder = orderRepository.save(
                Order
                        .builder()
                        .totalCharge(new BigDecimal("55.23"))
                        .date(today)
                        .build()
        );

        LocalDate yesterday = today.minusDays(1);
        Order firstPreviousDayOrder = orderRepository.save(
                Order
                        .builder()
                        .totalCharge(new BigDecimal("3.34"))
                        .date(yesterday)
                        .build()
        );

        BigDecimal todayIncome = firstTodayOrder.getTotalCharge().add(secondTodayOrder.getTotalCharge());
        BigDecimal yesterdayIncome = firstPreviousDayOrder.getTotalCharge();
        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .get("/api/report/daily"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[*].date").value(hasItems(today.toString(), yesterday.toString())))
                .andExpect(jsonPath(String.format("[?(@.date=='%s')].income", today.toString())).value(todayIncome.doubleValue()))
                .andExpect(jsonPath(String.format("[?(@.date=='%s')].income", yesterday.toString())).value(yesterdayIncome.doubleValue()))
                .andReturn();
    }
}
