package com.bymdev.vnovik.shoppingmgm.rest;

import com.bymdev.vnovik.shoppingmgm.ShoppingApplication;
import com.bymdev.vnovik.shoppingmgm.domain.Category;
import com.bymdev.vnovik.shoppingmgm.dto.CategoryDto;
import com.bymdev.vnovik.shoppingmgm.mapper.EntityMapper;
import com.bymdev.vnovik.shoppingmgm.repository.jpa.CategoryJpaRepository;
import com.bymdev.vnovik.shoppingmgm.rest.errors.ExceptionTranslator;
import com.bymdev.vnovik.shoppingmgm.service.CategoryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.Collections;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ShoppingApplication.class})
@Slf4j
public class CategoryResourceIntTest {
    private MockMvc mockMvc;

    @Autowired
    private CategoryResource categoryResource;

    @Autowired
    private CategoryJpaRepository categoryRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private EntityMapper mapper;

    @Autowired
    private ObjectMapper objectMapper;

    private Category defaultCategory;
    private CategoryDto defaultCategoryDto;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(categoryResource)
                .setControllerAdvice(new ExceptionTranslator())
                .build();

        categoryRepository.deleteAll();
        defaultCategory = Category.builder()
                .name("Default name")
                .build();
        defaultCategoryDto = mapper.categoryToDto(defaultCategory);
    }

    @After
    public void afterMethod() {
        defaultCategory = null;
    }

    @Test
    @Transactional
    public void shouldGetCategoryById() throws Exception {

        CategoryDto storedCategory = categoryService.save(defaultCategoryDto);
        mockMvc.perform(
                MockMvcRequestBuilders
                        .get("/api/categories/" + storedCategory.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(storedCategory.getId()))
                .andExpect(jsonPath("$.name").value(storedCategory.getName()));
    }

    @Test
    @Transactional
    public void shouldAddNewCategory() throws Exception {
        defaultCategoryDto.setProducts(Collections.emptyList());
        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/api/categories")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(defaultCategoryDto)))
                .andExpect(status().isOk())
                .andReturn();
        String responseBodyJson = mvcResult.getResponse().getContentAsString();
        Long storedCategoryId = JsonPath.parse(responseBodyJson).read("$.id", Long.class);
        Category persistedCategory = categoryRepository.findOne(storedCategoryId);

        Assert.assertNotNull(persistedCategory);
        Assert.assertEquals(defaultCategoryDto.getName(), persistedCategory.getName());
    }

    @Test
    @Transactional
    public void shouldUpdateExistedProduct() throws Exception {
        CategoryDto persistedCategory = categoryService.save(defaultCategoryDto);
        CategoryDto newCategory = CategoryDto
                .builder()
                .id(persistedCategory.getId())
                .name("Updated name")
                .build();

        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .put("/api/categories")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(newCategory)))
                .andExpect(status().isOk())
                .andReturn();

        Category updatedCategory = categoryRepository.findOne(persistedCategory.getId());

        Assert.assertNotNull(updatedCategory);
        Assert.assertEquals(newCategory.getName(), updatedCategory.getName());
        Assert.assertTrue(CollectionUtils.isEmpty(updatedCategory.getProducts()));
    }

    @Test
    @Transactional
    public void shouldDeleteCategoryWithoutProductsById() throws Exception {
        defaultCategory.setProducts(Collections.emptyList());
        Category storedCategory = categoryRepository.save(defaultCategory);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .delete("/api/categories/" + storedCategory.getId()))
                .andExpect(status().isOk());

        Category removedProduct = categoryRepository.findOne(storedCategory.getId());
        Assert.assertNull(removedProduct);
    }
}
