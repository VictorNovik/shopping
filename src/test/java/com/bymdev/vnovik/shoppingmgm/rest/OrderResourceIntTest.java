package com.bymdev.vnovik.shoppingmgm.rest;

import com.bymdev.vnovik.shoppingmgm.ShoppingApplication;
import com.bymdev.vnovik.shoppingmgm.domain.Order;
import com.bymdev.vnovik.shoppingmgm.domain.OrderItem;
import com.bymdev.vnovik.shoppingmgm.domain.Product;
import com.bymdev.vnovik.shoppingmgm.dto.OrderDto;
import com.bymdev.vnovik.shoppingmgm.dto.OrderItemDto;
import com.bymdev.vnovik.shoppingmgm.mapper.EntityMapper;
import com.bymdev.vnovik.shoppingmgm.repository.jpa.OrderItemJpaRepository;
import com.bymdev.vnovik.shoppingmgm.repository.jpa.OrderJpaRepository;
import com.bymdev.vnovik.shoppingmgm.repository.jpa.ProductJpaRepository;
import com.bymdev.vnovik.shoppingmgm.repository.search.OrderSearchRepository;
import com.bymdev.vnovik.shoppingmgm.rest.errors.ExceptionTranslator;
import com.bymdev.vnovik.shoppingmgm.service.OrderItemService;
import com.bymdev.vnovik.shoppingmgm.service.OrderService;
import com.bymdev.vnovik.shoppingmgm.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ShoppingApplication.class})
@Slf4j
public class OrderResourceIntTest {
    private MockMvc mockMvc;

    @Autowired
    private OrderResource orderResource;

    @Autowired
    private OrderItemJpaRepository orderItemRepository;

    @Autowired
    private OrderJpaRepository orderRepository;

    @Autowired
    private OrderItemService orderItemService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private ProductJpaRepository productRepository;

    @Autowired
    private OrderSearchRepository orderSearchRepository;

    @Autowired
    private EntityMapper mapper;

    @Autowired
    private ObjectMapper objectMapper;

    private Order defaultOrder;
    private OrderDto defaultOrderDto;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(orderResource)
                .setControllerAdvice(new ExceptionTranslator())
                .build();

        orderRepository.deleteAll();
        orderSearchRepository.deleteAll();

        Product product = addProduct(Product
                .builder()
                .price(new BigDecimal("4.21"))
                .sku("dd")
                .name("name")
                .build());

        List<OrderItem> orderItems = orderItemRepository.save(
                Collections.singletonList(
                        OrderItem
                                .builder()
                                .quantity(2)
                                .product(product)
                                .build()
                )
        );
        
        defaultOrder = Order
                .builder()
                .items(orderItems)
                .build();
        defaultOrderDto = mapper.orderToDto(defaultOrder);
    }

    @After
    public void afterMethod() {
        defaultOrder = null;
    }

    @Test
    @Transactional
    public void shouldGetOrderById() throws Exception {
        OrderDto storedOrder = orderService.save(defaultOrderDto);
        mockMvc.perform(
                MockMvcRequestBuilders
                        .get("/api/orders/" + storedOrder.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(storedOrder.getId()))
                .andExpect(jsonPath("$.totalCharge").value(storedOrder.getTotalCharge()))
                .andExpect(jsonPath("$.items.[*].id").value(hasItem(storedOrder.getItems().iterator().next().getId().intValue())));
    }

    @Test
    @Transactional
    public void shouldAddNewOrder() throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                post("/api/orders")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(defaultOrderDto)))
                .andExpect(status().isOk())
                .andReturn();
        String responseBodyJson = mvcResult.getResponse().getContentAsString();
        Long storedOrderId = JsonPath.parse(responseBodyJson).read("$.id", Long.class);
        Order persistedOrder = orderRepository.findOne(storedOrderId);

        Assert.assertNotNull(persistedOrder);
        Assert.assertNotNull(persistedOrder.getDate());
        Assert.assertNotNull(persistedOrder.getTotalCharge());
        Assert.assertNotNull(persistedOrder.getItems());
        Assert.assertTrue(persistedOrder.getItems().size() == 1);
    }

    @Test
    @Transactional
    public void shouldUpdateExistedOrder() throws Exception {
        OrderDto persistedOrder = orderService.save(defaultOrderDto);
        OrderItemDto extraOrderItem = orderItemService.save(
                OrderItemDto
                        .builder()
                        .quantity(4)
                        .product(persistedOrder.getItems().iterator().next().getProduct())
                        .build());
        OrderDto newOrder = OrderDto
                .builder()
                .id(persistedOrder.getId())
                .items(Arrays.asList(persistedOrder.getItems().iterator().next(), extraOrderItem))
                .build();

        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .put("/api/orders")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(newOrder)))
                .andExpect(status().isOk())
                .andReturn();

        Order updatedOrder = orderRepository.findOne(persistedOrder.getId());

        Assert.assertNotNull(updatedOrder);
        Assert.assertNotNull(updatedOrder.getTotalCharge());
        Assert.assertNotNull(updatedOrder.getDate());
        Assert.assertNotNull(updatedOrder.getItems());
        Assert.assertEquals(newOrder.getItems().size(), updatedOrder.getItems().size());
    }

    @Test
    @Transactional
    public void shouldDeleteOrderById() throws Exception {
        OrderDto storedOrder = orderService.save(defaultOrderDto);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .delete("/api/orders/" + storedOrder.getId()))
                .andExpect(status().isOk());

        Order removedOrder = orderRepository.findOne(storedOrder.getId());
        Assert.assertNull(removedOrder);
    }

    @Test
    @Transactional
    public void shouldFindOrderByProductNameFragment() throws Exception {
        Product canonProduct = addProduct(Product
                .builder()
                .sku("sku")
                .price(new BigDecimal("1.1"))
                .name("product canon")
                .build());

        Product nikonProduct = addProduct(Product
                .builder()
                .sku("sku2")
                .price(new BigDecimal("2.1"))
                .name("product nikon")
                .build());

        OrderItem canonOrderItem = addOrderItem(OrderItem
            .builder()
                .product(canonProduct)
                .quantity(2)
            .build());

        OrderItem nikonOrderItem = addOrderItem(OrderItem
                .builder()
                .product(nikonProduct)
                .quantity(1)
                .build());

        OrderDto storedOrder = orderService.save(
                OrderDto
                        .builder()
                        .items(Collections
                                .singletonList(mapper.orderItemToDto(nikonOrderItem)))
                        .build());

        orderService.save(
                OrderDto
                        .builder()
                        .items(Collections
                                .singletonList(mapper.orderItemToDto(canonOrderItem)))
                        .build());
        mockMvc
                .perform(post("/api/orders/_search/by-product-name")
                            .param("fragment", "nik*"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numberOfElements").value(1))
                .andExpect(jsonPath("$.elements.[*].id").value(hasItem(storedOrder.getId().intValue())))
                .andExpect(jsonPath("$.elements.[*].items.[*].id").value(hasItem(nikonOrderItem.getId().intValue())));

    }

    @Test
    @Transactional
    public void shouldFindMultipleOrdersByGeneralProductNameFragment() throws Exception {
        Product canonProduct = addProduct(Product
                .builder()
                .sku("sku")
                .price(new BigDecimal("1.1"))
                .name("product canon")
                .build());

        Product nikonProduct = addProduct(Product
                .builder()
                .sku("sku2")
                .price(new BigDecimal("2.1"))
                .name("product nikon")
                .build());

        OrderItem canonOrderItem = addOrderItem(OrderItem
                .builder()
                .product(canonProduct)
                .quantity(2)
                .build());

        OrderItem nikonOrderItem = addOrderItem(OrderItem
                .builder()
                .product(nikonProduct)
                .quantity(1)
                .build());

        OrderDto nikonOrder = orderService.save(
                OrderDto
                        .builder()
                        .items(Collections
                                .singletonList(mapper.orderItemToDto(nikonOrderItem)))
                        .build());

        OrderDto canonOrder = orderService.save(
                OrderDto
                        .builder()
                        .items(Collections
                                .singletonList(mapper.orderItemToDto(canonOrderItem)))
                        .build());
        mockMvc
                .perform(post("/api/orders/_search/by-product-name")
                        .param("fragment", "product"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numberOfElements").value(2))
                .andExpect(jsonPath("$.elements.[*].id").value(hasItem(nikonOrder.getId().intValue())))
                .andExpect(jsonPath("$.elements.[*].items.[*].id").value(hasItems(canonOrderItem.getId().intValue(), nikonOrderItem.getId().intValue())));

    }

    private OrderItem addOrderItem(OrderItem build) {
        return orderItemRepository.save(
                build
        );
    }

    private Product addProduct(Product build) {
        return productRepository.save(
                build
        );
    }
}
